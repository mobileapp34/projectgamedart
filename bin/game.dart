import 'dart:io';
import 'dart:math';
import 'package:test/expect.dart';
import 'player.dart';
import 'package:projectgame/projectgame.dart' as projectgame;
import 'player2.dart';

var player1 = Player1();
var player2 = Player2();
var count1 = 0;
var count2 = 0;
var element1;
var element2;
var card1 = player1.setHP1(player1.getHP1()+100);
var card2 = player1.setArmor1(player1.getArmor1()+20);
var card3 = player1.setAttack1(player1.getAttack1()+5);


void getRandom() {
  Random RandomCard = new Random();
  int testcard = RandomCard.nextInt(5);
}

void showWelcome() {
  print("================================");
  print("Welcome To Rock Paper Scrissor Card Game");
  print("================================");
}

void showHowtoplay() {print("================================");
  print("!How To Play and Rules! ");
  print("This Game is A Rock Paper Scrissor But In round Four You Can Use The Card");
  print("The Card Have 3 Card 1.Plus HP 2.Plus Armor 3.PlusAttack");
  print("The First You Can Input Your Name And Choose The Job and Choose Element For Your Player");
  print("================================");
}

void InputName() {
  print("Please Input Your name Player 1");
  var namep1 = stdin.readLineSync();
  print("================================");
  print("Please Input Your name Player 2");
  var namep2 = stdin.readLineSync();
  print("================================");
  print("Player1 name: ${namep1} Player2 name: ${namep2}");
  print("================================");
}

String getPlayer1RSP() {
  print("================================");
  print("Would you like Rock, Paper, or Scissors Player1: ?");
  print("================================");
  String? choose = stdin.readLineSync()?.toUpperCase();

  switch (choose) {
    case "R":
      return "Rock";
      break;
    case "P":
      return "Paper";
      break;
    case "S":
      return "Scissors";
      break;
    default:
      return "Quit";
      break;
  }
}

String getPlayer2RSP() {
  print("================================");
  print("Would you like Rock, Paper, or Scissors Player2 ?");
  print("================================");
  String? choose = stdin.readLineSync()?.toUpperCase();

  switch (choose) {
    case "R":
      return "Rock";
      break;
    case "P":
      return "Paper";
      break;
    case "S":
      return "Scissors";
      break;
    default:
      return "Quit";
      break;
  }
}

String? Winner(String getPlayer1RSP, String getPlayer2RSP) {
  if (getPlayer1RSP == getPlayer2RSP) {
    return "Draw";
  } else if (getPlayer1RSP == "Rock" && getPlayer2RSP == "Scissors") {
    if(player2.getArmor2() > 0){
      player2.setArmor2(player2.getArmor2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }else {
      player2.setHP2(player2.getHP2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }
    player2.showPlayer2();
    return "Player1 Wining";

  } else if (getPlayer1RSP == "Scissors" && getPlayer2RSP == "Paper") {
    if(player2.getArmor2() > 0){
      player2.setArmor2(player2.getArmor2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }else {
      player2.setHP2(player2.getHP2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }
    player2.showPlayer2();
    return "Player1 Wining";

  } else if (getPlayer1RSP == "Paper" && getPlayer2RSP == "Rock") {
    if(player2.getArmor2() > 0){
      player2.setArmor2(player2.getArmor2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }else {
      player2.setHP2(player2.getHP2() - player1.getAttack1());
      print("HP Player 2 And Armor is : ");
    }
    player2.showPlayer2();
    return "Player1 Wining";

  } else if (getPlayer2RSP == "Paper" && getPlayer1RSP == "Rock") {
    if(player1.getArmor1() > 0){
      player1.setArmor1(player1.getArmor1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }else {
      player1.setHP1(player1.getHP1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }
    player1.showPlayer1();
    return "Player2 Wining";

  } else if (getPlayer2RSP == "Rock" && getPlayer1RSP == "Scissors") {
    if(player1.getArmor1() > 0){
      player1.setArmor1(player1.getArmor1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }else {
      player1.setHP1(player1.getHP1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }
    player1.showPlayer1();
    return "Player2 Wining";

  } else if (getPlayer2RSP == "Scissors" && getPlayer1RSP == "Paper") {
    if(player1.getArmor1() > 0){
      player1.setArmor1(player1.getArmor1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }else {
      player1.setHP1(player1.getHP1() - player2.getAttack2());
      print("HP Player 1 And Armor is : ");
    }
    player1.showPlayer1();
    return "Player2 Wining";
  }
}

void WinGameP1(){
  if (player1.getHP1() == 0){
    print("================================");
    prints("TheWinner Is Player2");
    print("================================");
  }
}

void WinGameP2(){
  if (player2.getHP2() == 0){
    print("================================");
    prints("TheWinner Is Player1");
    print("================================");
  }
}

void getDamage() {
  player1.getArmor1();
}
void checkElement(){
  print("================================");
  print("Would you like To Fire Water Wind Earth Dark Light Player1: ");
  element1 = stdin.readLineSync();
  print("================================");
  print("Would you like To Fire Water Wind Earth Dark Light Player2: ");
  element2 = stdin.readLineSync();
  print("================================");
  if (element1 == "Fire" && element2 == "Water") {
    player1.setAttack1(player1.getAttack1() + 5);
  } else if (element1 == "Water" && element2 == "Wind") {
    player1.setAttack1(player1.getAttack1() + 5);
  } else if (element1 == "Wind" && element2 == "Earth") {
    player1.setAttack1(player1.getAttack1() + 5);
  } else if (element1 == "Earth" && element2 == "Fire") {
    player1.setAttack1(player1.getAttack1() + 5);
  } else if (element1 == "Light" && element2 == "Dark") {
    player1.setAttack1(player1.getAttack1() + 5);
  } else if (element2 == "Fire" && element1 == "Water") {
    player2.setAttack2(player2.getAttack2() + 5);
  } else if (element2 == "Water" && element1 == "Wind") {
    player2.setAttack2(player2.getAttack2() + 5);
  } else if (element2 == "Wind" && element1 == "Earth") {
    player2.setAttack2(player2.getAttack2() + 5);
  } else if (element2 == "Earth" && element1 == "Fire") {
    player2.setAttack2(player2.getAttack2() + 5);
  }else if (element1 == "Dark" && element2 == "Light") {
    player1.setAttack1(player1.getAttack1() + 5);
  }
}
void main(List<String> args) {
  showWelcome();
  showHowtoplay();
  InputName();
  player1.getselectClassPlayer1();
  player2.getselectClassPlayer2();
  checkElement();
  player1.showPlayer1();
  print("================================");
  player2.showPlayer2();
  while (true) {
     print("================================");
    print("Turn Player One");
    String player1Move = getPlayer1RSP();
    count1=count1+1;
     print("================================");
    print("Turn Player two");
    String player2Move = getPlayer2RSP();
    print("================================");
    print(Winner(player1Move, player2Move));
    print("================================");
   count2=count2+1;
    if(count1==4){
      print("================================");
    print("Are You Want To Use Card Player 1 You have A 3 Card for Card1 Card2 Card3: Yes or No");
    String? anwser1 = stdin.readLineSync();
    print("================================");
    if(anwser1 == "Yes"){
      print("================================");
    print("Choose The Card1 or Card2 or Card3");
    print("================================");
    String? card = stdin.readLineSync();
    if(card == "Card1"){
    card1;
    }else if (card == "Card2"){
     card2;
    }else if(card == "Card3"){
    card3;
    }
   }
   }
   if(count2==4){
    print("================================");
    print("Are You Want To Use Card Player 2 You have A 3 Card for Card4 Card5 Card6: Yes/No ");
    print("================================");
    String? anwser2 = stdin.readLineSync();
   if(anwser2 == "Yes"){
    print("================================");
    print("Choose The Card4 or Card5 or Card6");
    print("================================");
    String? card2 = stdin.readLineSync();
    if(card2 == "Card4"){
    var card4 = player2.setHP2(player2.getHP2()+100);
    }else if (card2 == "Card5"){
     var card5 = player2.setArmor2(player2.getArmor2()+20);
    }else if(card2 == "Card6"){
    var card6 = player2.setAttack2(player2.getAttack2()+5);
    }
   }
   }
    if (player1.getHP1() <= 0 || player2.getHP2() <= 0 ) {
      if (player2.getHP2() == 0){
      print("================================");
      prints("TheWinner Is Player1");
      print("================================");
  }   else if(player1.getHP1() == 0){
      print("================================");
      prints("TheWinner Is Player2");
      print("================================");
  }
      break;
    }
    
  }
  WinGameP1();
  WinGameP2();
}

